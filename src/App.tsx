import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Posts } from './pages/Posts';
import { Route, Switch } from 'react-router-dom';
import { PostCard } from './components/PostCard/PostCard';
import { SinglePostPage } from './pages/SinglePostPage';
import { LoginPage } from './pages/LoginPage';
import { NavBar } from './components/NavBar/NavBar';

const Layout: React.FunctionComponent = (props) => {
  return <div className="row">
    <div className="col">
      {props.children}
    </div>
  </div>
}

function App() {
  return (
    <>
      <NavBar/>
    <div className="container">
      <Layout>
        <Switch>
          <Route path="/" exact={true} render={() => <h1>home</h1>} />
          <Route path="/blog" exact={true} component={Posts} />
          <Route path="/blog/:post_id" component={SinglePostPage} />
          <Route path="/login" exact={true} component={LoginPage} />
          <Route path="/**" render={() => <h1>404 PAGE NOT FOUND</h1>} />
        </Switch>
      </Layout>
    </div>
    </>
  );
}

export default App;
