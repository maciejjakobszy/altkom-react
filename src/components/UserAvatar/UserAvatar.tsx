import React from 'react';
import { User } from '../../pages/User';

type P = {
    user: User
}


export const UserAvatar = (props: P) => {
    return <div>
<h4>
    <span style ={{width:'30px',height:'30px',backgroundColor:'tomato',
borderRadius:'50%',display:'block'}}></span>
<span>{props.user.name}</span>
</h4>
    </div>
}