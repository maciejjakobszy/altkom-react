import React from 'react'

type P = {
    onCommentAdd(commentText:string): void
}
type S = {
    commentText: string
}

export class CommentForm extends React.Component<P, S> {
    state: S = { commentText: '' }


    handleButtonClick = () => {
        console.log("click")
        this.props.onCommentAdd(this.state.commentText)
        this.setState({
            commentText: ' '
        })
    }

    handleInput = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        const value = event.target.value
        if (value.length <= 170) {
            this.setState({
                commentText: value
            })
        }
        console.log('input', value)
    }

    render() {
        return <div>
            Add comment
    <div className="input-group">
                <textarea 
                rows={3} cols={30} 
                className="form-control" 
                onChange={this.handleInput} 
                value={this.state.commentText} />
                <button 
                className="btn btn-info" 
                onClick={this.handleButtonClick}>+</button>
            </div>
            <span>{this.state.commentText.length} / 170 </span>
        </div>
    }

}