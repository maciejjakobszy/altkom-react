import React from 'react';
import { Post } from "../../pages/Post";
import { UserAvatar } from "../../components/UserAvatar/UserAvatar";
import { Link } from 'react-router-dom';

type P = {
    post: Post
    readmore?: boolean
}

export const PostCard = (props: P) => (
 
      <div className="card">
      <div className="card-body">
          {props.post.user && <UserAvatar user={props.post.user} />}
        <h5 className="card-title">{props.post.title}</h5>
        <p className="card-text">{props.post.body}</p>
        <h5 className="card-title">USER ID:{props.post.userId}</h5>
        <h5 className="card-title">POST ID:{props.post.id}</h5>
        {props.readmore && 
        <Link to={'/blog/' + props.post.id}>read more !!</Link>}
      </div>
      </div>
 
)