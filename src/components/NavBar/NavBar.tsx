import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { UserContext } from '../../UserContext';

export const NavBar = () =>
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
            <NavLink className="navbar-brand" to="/">Navbar</NavLink>
            <div>
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/" exact={true} activeClassName="active" >HOME</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/blog">BLOG</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/auctions">AUCTIONS</NavLink>
                    </li>
                </ul>
            </div>
            <UserContext.Consumer>{
                context => context.user ? <div className="text-light">

                    WELCOME: {context.user.name}

                    <span onClick={() => context.setUser(null)}> LOG OUT </span>

                </div> : <Link to="/login" className="text-light">LOG IN</Link>
            }
            </UserContext.Consumer>
        </div>
    </nav>