import React from 'react';
import { PostComment } from "../../pages/Post"
import { CommentForm } from '../CommentForm/CommentForm';

type P = {
    comments: PostComment[]
}

export const CommentsList = (props: P) => <>
    <ul className="list-unstyled">
{props.comments.map(comment =>
        <li className="media" key={comment.id}>
            <div className="media-body">
                <h5 className="mt-0 mb-1">{comment.body}</h5>
                <h5 className="mt-0 mb-1">{comment.name}</h5>
            </div>
        </li>)}
    </ul> 
</>


