import React from 'react';
import { PostCard } from '../components/PostCard/PostCard';
import { Post } from './Post';

type S = {
    posts: Post[]
};


export class Posts extends React.Component<{}, S>{

    state: S = {
        posts: []
    }

componentDidMount() {
    this.loadPosts()
}

    loadPosts = () => {
        // fetch('http://localhost:3000/posts?_limit=10')
        fetch('http://localhost:3000/posts?_expand=user')
            .then(r => r.json())
            .then((dane: Post[]) =>
                this.setState({
                    posts: dane
                })
            )
    }

    render() {
        return <div> 
           {/* <button onClick={this.loadPosts}></button> */}
            {this.state.posts.map(post => {
                return <PostCard post={post} key={post.id} readmore={true} />
            })}
        </div>
    }
}