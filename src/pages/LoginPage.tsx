import React from 'react'
import axios from 'axios'
import { User } from '../pages/User'
import { UserContext } from '../UserContext'
type S = {
    username: string,
    password: string,
}

export class LoginPage extends React.Component<{}, S> {

static contextType = UserContext

state = { username: '', password: ''}

    tryLogin = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        axios.get<User[]>('http://localhost:3000/users/', {
            params: {
                username: this.state.username,
                password: this.state.password
            }
            
        }).then(resp => {
            if (resp.data.length) {
                this.context.setUser(resp.data[0])
            }
            return console.log('server response', resp.data)
        })
    }
    
    handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        const fieldName = event.target.name
        const fieldValue = event.target.value
        switch (fieldName) {
            case 'username':
                this.setState({ username: fieldValue })
                break;
            case 'password':
                this.setState({ password: fieldValue })
                break;

        }


    }
    render() {
        return <>
            <form className="form-signin" onSubmit={this.tryLogin}>
                <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                <label className="sr-only">Email address</label>
                <input name="username" className="form-control" placeholder="Email address" onChange={this.handleInput} />
                <label className="sr-only">Password</label>
                <input name="password" type="password" id="inputPassword" className="form-control" placeholder="Password" onChange={this.handleInput} />
                <div className="checkbox mb-3">
                </div>
                <button className="btn btn-lg btn-primary btn-block" type="submit" >Sign in</button>
            </form>
        </>
    }

}





