import { User } from "./User";

export interface Post {
    userId:number;
    id: number;
    title: string;
    body?:string;
    user?: User
    comments?: PostComment[]
}

export interface PostComment {
    id: number;
    name: string;
    email:string;
    body:string;
}


