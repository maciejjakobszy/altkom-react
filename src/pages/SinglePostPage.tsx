import React from 'react'
import { Post } from './Post'
import { PostCard } from '../components/PostCard/PostCard'
import { RouteComponentProps } from 'react-router-dom'
import { CommentsList } from '../components/CommentsList/CommentsList'
import { CommentForm } from '../components/CommentForm/CommentForm'
import axios from 'axios'

type S = {
    post: Post | null
}
type P = {} & RouteComponentProps<{
    post_id: string
}>

export class SinglePostPage extends React.Component<P, S> {
    state: S = {
        post: null,
    }

    componentDidMount() {
        this.fetchPost(this.props.match.params.post_id)
        // this.loadPostsComments(this.props.match.params.comment_id)
    }
    fetchPost = (id: string) => {
        fetch(`http://localhost:3000/posts/${id}?_expand=user&_embed=comments`)
            .then(r => r.json())
            .then((dane: Post) => this.setState({ post: dane }))
    }
    
    addComment = (commentText: string) => {
        if (!this.state.post) { return }
        
        const comment = {
            postId: this.state.post.id,
            body: commentText,
            name: '',
            email: ''
        }
        
        axios.post('http://localhost:3000/comments', comment).then(data => {
            console.log(data)
            this.fetchPost(this.props.match.params.post_id)
        })
        
        
    }
    
    render() {
        return <div>
        {!this.state.post ? <div>loading</div> : null}
        {this.state.post && <PostCard post={this.state.post} />}
        <CommentForm onCommentAdd={this.addComment} />
        {this.state.post && this.state.post.comments &&
            <CommentsList comments={this.state.post.comments} />}
    </div>
}

}


