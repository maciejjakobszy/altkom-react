const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('data/data.json')
const middlewares = jsonServer.defaults()

server.use(middlewares)
server.get('/placki', (request, response) => {
  console.log(request.query)
  response.send('hello placki ')
})
console.log(middlewares)
server.use(router)
console.log(router)

server.listen(3000, () => {
  console.log('serv at 3000')
})